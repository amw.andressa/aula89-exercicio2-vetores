package application;

import entidades.Produto;

import java.util.Locale;
import java.util.Scanner;

// Fazer um programa para ler um número inteiro N e os dados (nome e preço) de N produtos.
// Armazene os N produtos em um vetor. Em seguida, mostrar o preço médio dos produtos.

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        System.out.println("Informe a quantidade de produtos: ");
        int quantidade = sc.nextInt();
        Produto[] vect = new Produto[quantidade];

        double soma = 0;
        for (int i = 0; i < vect.length; i++) {
            sc.nextLine();
            System.out.printf("Informe o nome do %dº produto: ", i+1);
            String nome = sc.nextLine();
            System.out.printf("Informe o preco do(a) " + nome +" : ", i+1);
            double preco = sc.nextDouble();
            vect[i] = new Produto(nome, preco);
            soma += vect[i].getPreco();
        }
        double media = soma / vect.length;
        System.out.printf("Media de precos: R$ %.2f", media);
    }
}
